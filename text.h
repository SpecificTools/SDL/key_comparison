#pragma once

#include <SDL2/SDL_ttf.h>

int render_text(char* message,
                int x,
                int y,
                TTF_Font* font,
                SDL_Renderer* renderer);

void extract_info(SDL_Keysym key,
                  char* code,
                  char* code_int,
                  char* scancode);

static char* stringfy_sym(SDL_Keycode code);
