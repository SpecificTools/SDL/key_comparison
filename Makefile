FONT_PATH = "/usr/share/fonts/TTF/DejaVuSansMono.ttf"

CC      = clang
CFLAGS  = -Wall -Wextra -g
LDFLAGS = -lSDL2 -lSDL2_ttf

HEADERS = $(wildcard *.h)
SOURCES = $(wildcard *.c)

OBJDIR = objs
OBJS   = $(patsubst %.c,$(OBJDIR)/%.o,$(SOURCES))

.PHONY: all
all: main

.PHONY: run
run: main
	./main $(FONT_PATH)

.PHONY: clean
clean:
	@rm -f main
	@rm -rf $(OBJDIR)

main: $(OBJS)
	$(CC) $(CFLAGS) $(LDFLAGS) $^ -o $@

$(OBJDIR)/%.o: %.c
	@mkdir -p $(OBJDIR)
	$(CC) -c $(CFLAGS) $^ -o $@
